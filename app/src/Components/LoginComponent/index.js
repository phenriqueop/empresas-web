import React, { Component } from 'react';
import './style.css';
import logo from './images/logo-home.png';

import api from '../../services/api';
import { Alert } from 'react-bootstrap';

import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import IconEmail from './images/ic-email.svg';
import IconLocked from './images/ic-cadeado.svg';
import { login } from '../../services/auth';

export default class LoginComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
          email: '',
          password: '',
          error: {
            visible: false,
            content: ''
          }
        };
      
      this.handleSubmit = this.handleSubmit.bind(this);
        
      }
    
    
      handleSubmit = async data => {
       const {email, password} = data;
       if(!email || !password) {
         this.setState({
           error: {
             visible: true,
             content: 'Preencha e-mail e senha para continuar'
           }
         })
       } else {
         try {
            const response = await api.post('/users/auth/sign_in', { email, password });
            const headers  = response.headers;
            login(headers["access-token"], headers.client, headers.uid);
            this.props.history.push("/home");
         } catch(err) {
           this.setState({
             error: {
               visible: true,
               content: "Houve um problema com o login, verifique suas credenciais."
             }
           })
         }
       }
    }
      render() {
          const schema = Yup.object().shape({
          email: Yup.string()
            .email('E-mail inválido')
            .required('E-mail obrigatório'),
          password: Yup.string()
            .min(4, 'Senha muito pequena')
            .max(20, 'Senha muito longa')
            .required('Senha Obrigatória'),
        });
    
       const errorSubmit = this.state.error;

    return (
        <div className="login-component">
            <div className="container">
                <div className="row">
                        <div className="col-12">
                            <img src={logo}
                            className="login-component--logo" alt="Logo da Ioasys">
                            </img>
                            <h2 className="login-component--title">
                            BEM-VINDO AO <br /> EMPRESAS
                            </h2>
                            <p className="login-component--sub-title">
                            Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
                            </p> 
                            <Formik
                                initialValues={{
                                    email: '',
                                    password: '',
                                }}
                                validationSchema={schema}
                                onSubmit={values => {
                                this.handleSubmit(values)
                                }}
                                >
                                    {({ errors }) => (
                                    <Form className="form-login">
                                        <Alert variant="danger" show={errorSubmit.visible}>
                                        { errorSubmit.content }
                                        </Alert>
                                        <div className="form-login--box">
                                            <img src={IconEmail} className="form-login--box---icon" alt="Ícone de Email" />
                                            <Field type="email" className="form-login--box---input" name="email" placeholder="E-mail" required />
                                            { errors.email ? <span>{errors.email}</span> : null}
                                        </div>
                                        <div className="form-login--box">
                                            <img src={IconLocked} className="form-login--box---icon" alt="Ícone da Senha" />
                                            <Field type="password" name="password" className="form-login--box---input" placeholder="Senha" required />
                                            {errors.password ? <span>{errors.password}</span> : null}
                                        </div>
                                        <button type="submit" className="form-login--submit">ENTRAR</button>
                                    </Form>

                            )}
      </Formik>
                        </div>
                    </div>
                </div>
        </div>
    )

    }
}


