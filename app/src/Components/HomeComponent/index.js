import React, { Component } from 'react'

import SearchIcon from './images/ic-search-copy.svg';
import logo from './images/logo-nav.png';
import close from './images/ic-close.svg';

import './styles.css';
import api from '../../services/api';
import { isEmptyChildren } from 'formik';

class HomeComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            openInput: false,
            searched: false,
            searching: false,
            typing: false, 
            typingTimeout: 0,
            enterprises: [],
            enterprise: {}
        };
      this.openInputHandler = this.openInputHandler.bind(this);
      this.inputChangeHandler = this.inputChangeHandler.bind(this);
    }

    inputChangeHandler = (e) => {
    this.setState({
            searching: true,
            enterprises: [],
            enterprise: {}
    });
    const self = this;
    const name  = e.target.value;
    if (self.state.typingTimeout) {
        clearTimeout(self.state.typingTimeout);
    } 
    if(e.target.value === '' && this.state.searched){
        this.setState({
            enterprises: [],
            searching: false
        })
    }
    if(e.target.value !== ''){
     self.setState({
        typing: false,
        typingTimeout: setTimeout(function () {
           api.get('/enterprises', {
               params: {
                enterprise_types: 1,
                name: name
               }        
           })
           .then(response => {
               self.setState({
                   searched: true,
                   enterprises: response.data.enterprises,
                   searching: false
               });
            })
        }, 3000)
     });
    }
    }

    openEnterpriseHandler = (id) => {
       const enterprise = this.state.enterprises.filter( function( elem, i, array ) {
            return array[i].id === id;
        } );
        this.setState({
            enterprises: [],
            enterprise: enterprise[0]
        });
        
    }

    openInputHandler = (e) => {
        this.setState({
            openInput: !this.state.openInput,
            enterprises: [],
            enterprise: {},
            searched: false
        });
    }
    

    closeFormHandler = () => {
        document.getElementsByClassName('header--input')[0].value='';
        this.openInputHandler();
    }
    render () {
        return (
            <div>
                 <header className="header">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-12">
                                <img src={logo} alt="Logo da Oasys" className={ this.state.openInput ? 'd-none' : '' }  />
                                <input type="text" onChange={e => this.inputChangeHandler(e)} className={`header--input ${this.state.openInput ? 'header-input-active' : ''}`} name="search" placeholder="Pesquisar" /> 
                                <a href="#" onClick={this.openInputHandler}>
                                  <img src={SearchIcon} alt="Botão Procurar" className={`header--icon header--icon-lupa ${this.state.openInput ? 'header--icon-lupa-left' : ''}`}/>
                                </a>    
                                <a href="#" onClick={e => this.closeFormHandler(e)}>
                                  <img src={close} alt="Botão Fechar" className={`header--icon header-icon--close ${this.state.openInput ? 'header-icon--close-active' : ''}`}/>
                                </a>      
                            </div>
                        </div>
                    </div>
                </header>
                <div className="content">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-12">
                            <h3 className="content--title">
                      { !this.state.openInput &&
                         'Clique na busca para iniciar.'
                      }
                      { (this.state.openInput && this.state.enterprises.length === 0 && this.state.searching !== true && Object.entries(this.state.enterprise).length === 0) &&
                         'Nenhuma empresa foi encontrada.'
                       }
                       {  this.state.searching ? 'Procurando...' : '' }
                      </h3> 
                   
                    {
                        (this.state.enterprises.length > 0 && this.state.openInput) &&
                        <div className="content--enterprises">
                            <ul className="content--enterprises---list">
                                {this.state.enterprises.map((enterprise) => 
                                    <li className="content--enterprises---item" key={enterprise.id} onClick={() => this.openEnterpriseHandler(enterprise.id)}>
                                        <span className="content-enterprises---type">
                                            {enterprise.enterprise_name.charAt(0)}{enterprise.enterprise_type.id}
                                        </span>
                                        <div className="content-enterprises---desc">
                                            <h3 className="content-enterprises---name">{enterprise.enterprise_name}</h3>
                                            <h4 className="content-enterprises---business">{enterprise.enterprise_type.enterprise_type_name}</h4>
                                            <p className="content-enterprises---country">{enterprise.country}</p>
                                        </div>
                                    </li>

                                )
                            }
                            </ul>
                        </div>
                    }

                    { ((Object.entries(this.state.enterprise).length !== 0 && this.state.enterprise.constructor === Object) && this.state.openInput) &&
                        <div className="content--enterprises">
                            <ul className="content--enterprises---list">
                                    <li className="content--enterprises---item list-block" key={this.state.enterprise.id} onClick={() => this.openEnterpriseHandler(this.enterprise.id)}>
                                        <span className="content-enterprises---type type-big">
                                            {this.state.enterprise.enterprise_name.charAt(0)}{this.state.enterprise.enterprise_type.id}
                                        </span>
                                        <div className="content-enterprises---desc desc-detail">
                                            <p className="content-enterprises---desc-text">{this.state.enterprise.description}</p>
                                        </div>
                                    </li>                
                            </ul>
                        </div>
                    }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default HomeComponent