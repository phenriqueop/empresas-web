import React from 'react';

import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Login from './Components/LoginComponent';
import HomeComponent from './Components/HomeComponent';
import DetailEnterpriseComponent from './Components/DetailEnterpriseComponent';
import { isAuthenticated } from "./services/auth";


const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
      {...rest}
      render={props =>
        isAuthenticated() ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )
      }
    />
  );

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Login}/>
            <PrivateRoute path="/home" component={HomeComponent} />
            <PrivateRoute path="/enterprise/:id" component={DetailEnterpriseComponent} />
        </Switch>
    </BrowserRouter>
);  

export default Routes;