import React from 'react';

import 'bootstrap/scss/bootstrap.scss';
import "./global.css";

import Routes from './routes';


const App = () => {
    return (
       <div className="app">
         <Routes />
       </div>
    );
}

export default App;

