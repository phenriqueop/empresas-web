export const ACCESS_TOKEN = "";
export const isAuthenticated = () => localStorage.getItem("access-token") !== null;
export const getToken = () => localStorage.getItem(ACCESS_TOKEN);
export const login = (accessToken, client, uid) => {
  localStorage.setItem("access-token", accessToken);
  localStorage.setItem("client", client);
  localStorage.setItem("uid", uid);
};
export const logout = () => {
  localStorage.removeItem(ACCESS_TOKEN);
};