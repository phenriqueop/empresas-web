import axios from 'axios';

import { isAuthenticated } from './auth';

const api = axios.create({baseURL: 'http://empresas.ioasys.com.br/api/v1'});

api.interceptors.request.use(async config => {
    
    if ( isAuthenticated()) {
      config.headers.get['access-token'] = localStorage.getItem('access-token');
      config.headers.get['client'] = localStorage.getItem('client');
      config.headers.get['uid'] = localStorage.getItem('uid');
      axios.defaults.headers.get['Content-Type'] = 'application/json';
    }
    return config;
  });
  

export default api;